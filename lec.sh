#!/bin/sh

builddate=$(grep "lastBuildDate" lectionary-daily.xml | sed -e 's/^[^<lastBuildDate>]*<lastBuildDate>//' -e 's/<\/lastBuildDate>//' | cut -d ' ' -f -4)
#echo "$builddate"

sysdate=$(date +'%a, %d %b %G')
#Fri, 09 Apr 2021

if [ "$builddate" != "$sysdate" ]
then
    rm -f "$PWD/lectionary-daily.xml"
    wget https://www.catholic.org/xml/rss_dailyreadings.php -O lectionary-daily.xml
# else
#     printf "We already have today's lectionary\n\n"
fi

#lecdate=$(date --date="-7 day" +'%B %d')
lecdate=$(date +'%B %d')
#April 09

lectionary=$(grep -B 1 -A 6 "$lecdate" lectionary-daily.xml | grep description | sed -e 's/^[^<description>]*<description>//' -e 's/<\/description>//')

printf "%s\n\n" "$sysdate"
#Fri, 09 Apr 2021

printf "%s\n\n" "$lectionary"

if [ "$1" = -v ]
then
    echo "$lectionary" | tr ',' '\n' | sed -e 's/^[^:]*://' -e 's/Acts of Apostles/Acts/' -e 's/Ecclesiasticus \/ Sirach/Sirach/' | xargs -d'\n' -I % sh -c 'vul -W % | cat; echo ""'
else
    echo "$lectionary" | tr ',' '\n' | sed -e 's/^[^:]*://' -e 's/Acts of Apostles/Acts/' -e 's/Ecclesiasticus \/ Sirach/Sirach/' | xargs -d'\n' -I % sh -c 'kjv -W % | cat; echo ""'
fi


# Daily Catholic Lectionary

Uses https://www.catholic.org/xml/rss_dailyreadings.php for the
lectionary.

`./lec.sh` will give you today's lectionary with kjv.

`./lec.sh -v` for the latin vulgate output.

## Requirements

- wget

- xargs

- kjv https://git.larbs.xyz/luke/kjv/

- vul (optional) https://git.larbs.xyz/luke/vul/

### Using a different RSS feed

The downloaded feed is in the repo for inspection;
https://www.catholic.org/xml/rss_dailyreadings.php uses a format like
this:

> Reading 1: Acts of Apostles 4:1-12, Responsorial Psalm: Psalms 118:1-2, Responsorial Psalm: Psalms 118:4, Responsorial Psalm: Psalms 118:22-27, Gospel: John 21:1-14

So long as you can get an output like that from the feed's description
you should be fine.
